"""Make manual edits to the gnome-shell.css for "Black" variant."""
import sys
from pathlib import Path

fix_pop_style = """
stage { font-size: 11pt; color: #DADADA; }


.popup-menu-item:checked {
  color: #ffffff !important
}

.popup-menu-item:focus {
  color: #000000 !important
}
"""

bg = "background-color:"
running_dot = " ".join(
    [
        "#dashtodockContainer.running-dots",
        ".app-well-app:hover",
        ".app-well-app-running-dot",
    ]
)
app_dir_hover = " ".join(
    [
        ".app-well-app.app-folder:hover >",
        ".overview-icon,",
        ".app-well-app.app-folder:focus >",
        ".overview-icon",
    ]
)
app_dir = ".app-well-app.app-folder > .overview-icon"
b = "border: 1px solid rgba(0, 0, 0, 0.5);"
border = (
    ".app-folder-dialog" " { border-radius: 2px; border: 1px solid rgba(0, 0, 0, 0.5);"
)
padding = "padding: 12px 0; }"

replacements = {
    f"{running_dot} {{ {bg} #000000; }}": f"{running_dot} {{ {bg} #ffffff; }}",
    f"{app_dir_hover} {{ {bg} #2a2a2a; }}": f"{app_dir_hover} {{ {bg} #6f6f6f; }}",
    f"{app_dir} {{ {bg} #0e0e0e; {b} }}": f"{app_dir} {{ {bg} #262626; {b} }}",
    f"{border} {bg} #0e0e0e; {padding}": f"{border} {bg} #2f2f2f; {padding}",
    "0b0b0b": "111111",
    "stage { font-size: 11pt; color: #D3DAE3; }": fix_pop_style,
}

css_path = Path(sys.argv[0])
gnome_shell_css = css_path.read_text()

for old, new in replacements.items():
    gnome_shell_css = gnome_shell_css.replace(old, new)

css_path.write_text(gnome_shell_css)
